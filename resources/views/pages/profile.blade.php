@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Profile</div>

                <div class="panel-body">
                    <p> {{ $myname }} </p>
                    <h1>
                        Your age is
                        {{-- Custom directive --}}
                        @age([1993,9,3])
                    </h1>
                    <h3>
                       @sayHello('Nasir Fardoush')
                    </h3>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
