@extends('layouts.app')
@section('content')
	<div class="row" >
		<div class="col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-2" >
			<h2>Total users( {{$users->total()}})</h2>
			<h3>Total users in this page( {{$users->count()}})</h3>
			<h3>Current page number( {{$users->currentPage()}})</h3>
			<h3>Current page first item id( {{$users->firstItem()}})</h3>

			<ul class="list-group"  >
				@forelse($users as $user)
					<li class="list-group-item" style="margin-top:20px;" >
						<span>
							{{ $user->name }}
						</span>
						<span class="pull-right clearfix" >
							Joined({{ $user->created_at->diffForHumans() }})
							<button class="bt btn-xs btn-primary" >Follow</button>
						</span>
					</li>
				@empty
					<p>No users avilable</p>
				@endforelse
				
				{{ $users->links() }}
			</ul>
		</div>
	</div>

@endsection
